# Тестовое задание для retech-labs 

## Как запустить проект

* `git clone git@gitlab.com:itksb/retech-labs-test.git retech-labs-test`
* `cd retech-labs-test`
* `pipenv install`
* set database settings in server/settings.py
* `python manage.py migrate`
* `python manage.py loaddata main_app`
* `python manage.py runserver`
* open browser http://localhost:8000/admin and login with admin@itksb.com:superadmin
* api: http://localhost:8000/todos_api/v1/

## Задание

1. Необходимо реализовать ToDo список и позволить пользователю проводить CRUDоперации
над ним.
2. ToDo списки относятся к различным организациям.
3. Чтобы получить доступ к списку, пользователь должен авторизоваться.
4. Авторизация должна происходить с помощью email и пароля.
5. Возможна регистрация пользователя (сама логика регистрации не входит в задание) с
привязкой к одной и более организации, но с одним и тем же email пользователя.
6. Пользователь должен авторизовываться только в одной организации. То есть, чтобы
получить доступ к ToDo списку другой организации, пользователь должен
переавторизоваться в необходимой ему организации. Фронтенда не нужно.
7. ! Для авторизации и модели пользователя обязательно использовать
'django.contrib.auth'.
8. ! Необходимо использовать DRF serializers вместо django-forms.
9. Стек технологий:
- python2/python3
- django 1.10
- django-rest-framework
- любая СУБД
10. Форма готового ТЗ – публикация, ссылка на git репозиторий или архив.

