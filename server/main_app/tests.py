from django.contrib.auth import get_user_model
from django.test import TestCase

from .models import Todo, Company, TodoEditor


class TodoModelTest(TestCase):
    TODO_DESCR = 'test case'

    @classmethod
    def setUpTestData(cls):
        company = Company.objects.create(
            id=1,
            name='company',
        )

        user = get_user_model()(
            id=1,
            username='username',
            email='test@test.ru',
            password='123',
        )

        todoEditor = TodoEditor(
            user_id=user.id,
        )

        Todo.objects.create(
            title='todo 001',
            description=TodoModelTest.TODO_DESCR,
            company_id=company.id,
            editor_id=todoEditor.user_id
        )

    def test_title_content(self):
        todo = Todo.objects.get(id=1)
        expected_object_name = f'{todo.title}'
        self.assertEquals(expected_object_name, 'todo 001')

    def test_description_content(self):
        todo = Todo.objects.get(id=1)
        expected_object_descr = f'{todo.description}'
        self.assertEquals(expected_object_descr, TodoModelTest.TODO_DESCR)

    def test_company_content(self):
        todo = Todo.objects.get(id=1)
        expected_company_id = f'{todo.company_id}'
        self.assertEquals(expected_company_id, '1')

    def test_editor_content(self):
        todo = Todo.objects.get(id=1)
        expected_editor_id = f'{todo.editor_id}'
        self.assertEquals(expected_editor_id, '1')
