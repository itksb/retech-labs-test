from django.db import models
from django.utils.translation import ugettext_lazy as _


class AbstractEntity(models.Model):
    date_created = models.DateTimeField(
        _('date_created'),
        auto_now_add=True
    )
    date_updated = models.DateTimeField(
        _('date_updated'),
        auto_now=True,
    )

    class Meta:
        abstract = True
