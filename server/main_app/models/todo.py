from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from server.main_app.models import Company
from .abstractmodel import AbstractEntity

UserModel = get_user_model()


class TodoEditor(AbstractEntity):
    """
    A class representation of the Todo publisher model
    One to one relationship with User
    """
    user = models.OneToOneField(
        UserModel,
        on_delete=models.CASCADE,
        primary_key=True
    )

    companies = models.ManyToManyField(
        Company,
        through='TodoEditorCompanyMembership'
    )

    def __str__(self):
        return "%s the todo editor" % self.user.username


class Todo(AbstractEntity):
    """
    A class representation of the Todo model
    """
    title = models.CharField(
        _('title'),
        max_length=200,
        null=False
    )
    description = models.TextField(
        _('description'),
        max_length=255,
        null=True,

    )

    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        null=False
    )

    editor = models.ForeignKey(
        TodoEditor,
        on_delete=models.PROTECT,
        null=False
    )

    def __str__(self):
        """A string representation of the model."""
        return self.title

    class Meta:
        ordering = ('title',)


class TodoEditorCompanyMembership(AbstractEntity):
    """
    A class representation of many-to-many relationship between
    todo editors and companies
    """
    editor = models.ForeignKey(
        TodoEditor,
        on_delete=models.CASCADE,
        null=False,
    )
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        null=False,
    )

    class Meta:
        unique_together = (("editor", "company"),)
