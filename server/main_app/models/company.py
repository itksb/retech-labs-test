from django.db import models
from django.utils.translation import ugettext_lazy as _

from .abstractmodel import AbstractEntity


class Company(AbstractEntity):
    """
    A class representation of the Company model
    """
    name = models.CharField(
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer.'),
        null=False
    )

    def __str__(self):
        """A string representation of the model."""
        return self.name

    class Meta:
        verbose_name_plural = 'Companies'
