from django.apps import AppConfig


class MainAppConfig(AppConfig):
    name = 'server.main_app'
