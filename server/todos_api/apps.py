from django.apps import AppConfig


class TodosApiConfig(AppConfig):
    name = 'server.todos_api'
