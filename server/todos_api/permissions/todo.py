from rest_framework.permissions import BasePermission


class IsTodoEditor(BasePermission):
    """
    Custom permission to only allow editors of the todo to CRUD it
    """

    def has_permission(self, request, view):
        return request.user.todoeditor is not None

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if user company is the same as todo item company
        """
        return (
            request.auth is not None
            and
            obj.company == request.auth.company
        )
