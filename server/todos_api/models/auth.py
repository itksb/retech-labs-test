import binascii
import os

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from server.main_app.models import Company


@python_2_unicode_compatible
class CompanyCoupledToken(models.Model):
    """
    The  authorization token model.
    """
    key = models.CharField(_("Key"), max_length=40, primary_key=True)
    # ManyToOne relationship,
    # see https://docs.djangoproject.com/en/2.1/topics/db/examples/many_to_one/#many-to-one-relationships
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='user_token',
        on_delete=models.CASCADE, verbose_name=_("User")
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)

    company = models.ForeignKey(
        Company, related_name='company',
        on_delete=models.CASCADE, verbose_name=_("Company"),
        null=False
    )

    class Meta:
        verbose_name = _("Token")
        verbose_name_plural = _("Tokens")

    def save(self, *args, **kwargs):
        if self.key:
            self.delete()

        self.key = self.generate_key()
        return super(CompanyCoupledToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key
