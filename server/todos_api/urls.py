from django.conf.urls import url
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('companies/', views.ListCompany.as_view()),
    url(r'^companies/(?P<pk>[0-9]+)/?$', views.DetailCompany.as_view()),
    path('login/', views.ObtainExpiringAuthToken.as_view()),
    url(r'^$', views.index),
    url(r'^todos/?$', views.TodoList.as_view(), name='todo-list'),
    url(r'^todos/(?P<pk>[0-9]+)/?$', views.TodoDetail.as_view(), name='todo-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
