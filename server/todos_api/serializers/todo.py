from rest_framework import serializers

from server.main_app.models.todo import Todo


class TodoSerializer(serializers.ModelSerializer):
    """
    Read-only field, used for serialized representations, not used for updating.
    Editor.id attribute is used to populate a field
    """
    editor = serializers.ReadOnlyField(source='editor.id')

    """
    Read-only field, used for serialized representations, not used for updating.
    Company.id attribute is used to populate a field
    """
    company = serializers.ReadOnlyField(source='company.id')

    class Meta:
        fields = (
            'id',
            'title',
            'description',
            'company',
            'editor',
            'date_created',
            'date_updated'
        )
        model = Todo
