from rest_framework import serializers

from server.main_app.models.company import Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'name'
        )
        model = Company
