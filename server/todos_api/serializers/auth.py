from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.compat import authenticate

from server.main_app.models.todo import TodoEditor


class AuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField(label=_("Email"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    company_id = serializers.CharField(label=_("Company"))

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        company_id = attrs.get('company_id')

        if email and password and company_id:
            user = authenticate(
                request=self.context.get('request'),
                email=email, password=password, company_id=company_id)
            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')

            try:
                todoEditor = TodoEditor.objects.get(user_id=user.id)
            except:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')

            if not todoEditor.companies.filter(pk=company_id).exists():
                msg = _('You account is not related to company_id=%s' % company_id)
                raise serializers.ValidationError(msg, code='authorization')


        else:
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
