from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


@api_view(['GET'])
def index(request, format=None):
    return Response({
        'users': reverse('users:user-list', request=request, format=format),
        'todos': reverse('todos:todo-list', request=request, format=format),
    })
