from rest_framework import generics

from server.main_app.models import Company
from ..serializers.company import CompanySerializer


class ListCompany(generics.ListAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class DetailCompany(generics.RetrieveAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
