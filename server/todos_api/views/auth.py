import datetime

from pytz import utc
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response

from ..models.auth import CompanyCoupledToken
from ..serializers.auth import AuthTokenSerializer


class ObtainExpiringAuthToken(ObtainAuthToken):
    """
    Login view for API
    """

    def post(self, request, **kwargs):
        serializer = AuthTokenSerializer(data=request.data)

        if serializer.is_valid():
            user = serializer.validated_data['user']
            company_id = serializer.validated_data['company_id']

            token, created = CompanyCoupledToken.objects.get_or_create(
                user=user,
                company_id=company_id,
            )
            if not created:
                # update the created time of the token to keep it valid
                token.created = datetime.datetime.utcnow().replace(tzinfo=utc)
                token.save()

            return Response({'token': token.key})

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
