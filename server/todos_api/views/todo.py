from rest_framework import generics
from rest_framework import permissions

from server.main_app.models import Todo
from server.main_app.models.todo import TodoEditor
from ..permissions import IsTodoEditor
from ..serializers.todo import TodoSerializer


class TodoList(generics.ListCreateAPIView):
    """
    Represents list of todo items
    """
   # queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = [permissions.IsAuthenticated, IsTodoEditor]

    def perform_create(self, serializer):
        """
        Associates current todo editor (user)  and editor company with created todo item
        """
        todo_editor = TodoEditor.objects.get(pk=self.request.user.pk)
        company = self.request.auth.company
        serializer.save(editor=todo_editor, company=company)

    def get_queryset(self):
        todoeditor = self.request.user.todoeditor
        return Todo.objects.all()

class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Represents detail information ot the todo item
    """
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = [permissions.IsAuthenticated, IsTodoEditor]
